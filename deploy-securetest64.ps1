Write-Host "Building for XPAY"
ng build --base-href /clientcopy/ --configuration=securetest64
Write-Host "Wiping \\ioits1web01\raptor\clientcopy..."
Remove-Item "\\ioits1web01\raptor\clientcopy\*.*" -recurse -Force
Write-Host "Done."
Write-Host "Pushing \\ioits1web01\raptor\clientcopy..."
Copy-Item -Path "dist\clientcopy\*" -Destination "\\ioits1web01\raptor\clientcopy" -recurse -Force
Write-Host "Done."
