
export class ResponseModel {
    CopyFromTo: number;
    Source: string;
    Destination: string;
    Division: string;
    Year: number;
    CopyWasSuccessful: boolean;
    BatchFileInfo: string;
    public DivBlocked: boolean;

    constructor() {
        this.CopyWasSuccessful = false;
    }

    SetValuesModel(copyfromto: number, division: string, year: number, copywassuccessful: boolean, fullpath: string, source: string, destination: string, divblocked: boolean) {
        this.CopyFromTo = copyfromto;
        this.Source = source;
        this.Destination = destination;
        this.Division = division;
        this.Year = year;
        this.CopyWasSuccessful = copywassuccessful;
        this.BatchFileInfo = fullpath;
        this.DivBlocked = divblocked;
    }
}
