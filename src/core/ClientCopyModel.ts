export class ClientCopy {
  constructor(
    public Source: string,
    public Destination: string,
    public Division: string,
    public Year: number,
  ) {}

  public GetCopyFromToNumber(): number {
    if (this.Source == 'prod' && this.Destination == 'test')
      return 0;
    else if(this.Source == 'prod' && this.Destination == 'xpay')
      return 1;
    else if(this.Source == 'prod' && this.Destination == 'qa')
      return 2;
    else if(this.Source == 'test' && this.Destination == 'qa')
      return 3;
    else
      return 4;
  }

  public IsValid(): boolean {
    const max: number = new Date().getFullYear();
    const min: number = max -4;
    const currentYear = new Date().getFullYear;
    if(this.Source != undefined && this.Destination != undefined && this.Division.length == 5) {
      if (this.Year != undefined && this.Year >= min && this.Year <= max)
        return true;
      else
        return false;
    }
    else
      return false;
  }
}
