import { Component, OnInit } from '@angular/core';
import { ClientCopyService } from './../services/client-copy.service';

@Component({
  selector: 'app-spinner',
  templateUrl: './spinner.component.html',
  styleUrls: ['./spinner.component.sass'],
})

export class SpinnerComponent implements OnInit {

  showSpinner = false;
  constructor(private ccService: ClientCopyService) { }

  ngOnInit() {
    this.ccService.activateSpinner$.subscribe((show) => this.showSpinner = show);
  }

}
