import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, Observer } from 'rxjs';
import { RequestModel } from 'src/core/RequestModel';
import { ResponseModel } from 'src/core/ResponseModel';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class ClientCopyService {

  private _clientCopyResponse: ResponseModel;
  private _clientCopyResponse$ = new BehaviorSubject(this._clientCopyResponse) as BehaviorSubject<ResponseModel>;

  private _activateSpinner$: Observer<boolean>;
  public activateSpinner$: Observable<boolean> = new Observable((obs) => this._activateSpinner$ = obs);

  constructor( private http: HttpClient) {

  }

  public showSpinner() {
    try {
      this._activateSpinner$.next(true);
    } catch { }
  }

  public hideSpinner() {
    try {
      this._activateSpinner$.next(false);
    } catch { }
  }

  public clientCopyResponseSubject$() {
    return this._clientCopyResponse$;
  }

  public performClientCopy( requestModel: RequestModel): Promise<boolean> {
    this.showSpinner();
    const promise = new Promise<boolean>((resolve) => {
      const requestUrl = environment.ApplicationAPIBaseUrl + 'PerformClientCopyAsync';
      this.http.post(requestUrl, requestModel).toPromise().then( (result) => {
        const data = result as ResponseModel;
        if (data) {
          this._clientCopyResponse = data;
          this._clientCopyResponse$.next(this._clientCopyResponse);
          resolve(true);
        } else {
          console.log('Invalid result', result);
          resolve(false);
        }
        this.hideSpinner();
      }).catch((err) => {
        console.log(err);
        resolve(false);
        this.hideSpinner();
      });

    });

    return promise;
  }
}
