import { TestBed } from '@angular/core/testing';

import { ClientCopyService } from './client-copy.service';

describe('ClientCopyService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ClientCopyService = TestBed.get(ClientCopyService);
    expect(service).toBeTruthy();
  });
});
