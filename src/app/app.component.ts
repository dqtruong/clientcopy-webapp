import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { reject } from 'q';
import { Subscription } from 'rxjs';
// import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ClientCopy } from 'src/core/ClientCopyModel';
import { RequestModel } from 'src/core/RequestModel';
import { ResponseModel } from 'src/core/ResponseModel';
import { ClientCopyService } from './services/client-copy.service';
// import { $ } from 'protractor';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass'],
})

export class AppComponent implements OnInit, OnDestroy {

  processing = false;
  clientCopyModel: ClientCopy;
  successText = undefined;
  requestModel: RequestModel;
  responseModel: ResponseModel;
  title = 'Client Copy';
  sources = [];
  destinations = [];
  alertAreaText: string;
  alertClass: string;
  currentYear = new Date().getFullYear();
  responseModelSub: Subscription;
  // to update blocked list, add or remove the line below
  // userForm: FormGroup;
  // sourceControl: FormControl;
  // destinationControl: FormControl;
  // yearControl: FormControl;
  // divisionControl: FormControl;

  constructor(private http: HttpClient, private clientCopyService: ClientCopyService) {}

  ngOnInit() {
    this.sources = [
        { id: 'prod', text: 'Prod' },
        { id: 'test', text: 'Test' }
    ];
    this.destinations = [
        { id: 'qa', text: 'QA', disabled: false },
        { id: 'xpay', text: 'XPay', disabled: false },
        { id: 'test', text: 'Test', disabled: false }
    ];
    this.clientCopyModel = new ClientCopy('', '', '', this.currentYear);
    this.responseModel = new ResponseModel();
    // this.createFormControls();
    // this.creaForm();
    this.clientCopyModel.Source = 'prod';
    this.clientCopyModel.Destination = 'test';
    // here we subscribe to data responsed for client copy request
    this.responseModelSub = this.clientCopyService.clientCopyResponseSubject$().subscribe((modelData) => {
      if (modelData && this.responseModel !== modelData) { // when new data is returned set it to the local model variable
        this.responseModel = modelData;
        this.ResponseSuccessAction(); // display success onces complete
      }
    });
  }

  ngOnDestroy(): void {
    this.responseModelSub.unsubscribe();
  }

  SelectOption(id: string) {
    const destinationVal = this.clientCopyModel.Destination;
    const sourceVal = this.clientCopyModel.Source;
    if (sourceVal === 'test') {
      if (destinationVal === 'test') {
        this.clientCopyModel.Destination = undefined;
      }
      this.destinations.find((v) => v.id === 'test').disabled = true;
    } else {
      this.destinations.find((v) => v.id === 'test').disabled = false;
    }
  }

  ExecuteCopy() {
    if (this.clientCopyModel.IsValid()) {
        this.alertAreaText = 'Please wait while processing...';
        this.alertClass = 'alert-info';
        this.successText = undefined;
        // tslint:disable-next-line: max-line-length
        const request = new RequestModel(this.clientCopyModel.GetCopyFromToNumber(), this.clientCopyModel.Division, this.clientCopyModel.Year);
        this.processing = true;
        this.clientCopyService.performClientCopy(request).then((success) => {
          this.processing = false;
          if (!success) {
          this.ResponseFailAction();
        }
      });
    } else {
      this.FormValidationMessage();
    }
  }

  ResetForm() {
    this.clientCopyModel = new ClientCopy('', '', '', new Date().getFullYear());
    this.alertAreaText = '';
    this.successText = undefined;
    this.alertClass = '';
  }

  FormValidationMessage() {
    this.alertAreaText = 'Please supply required info, valid years are from ' + (this.currentYear - 4) + ' to ' + this.currentYear;
    this.alertClass = 'alert-danger';
    this.successText = undefined;
  }

  ResponseSuccessAction() {
    this.alertAreaText = this.responseModel.DivBlocked ? "This division is blocked." : "Copy was successful.";
    // this.successText = '<b>From:</b> ' + this.responseModel.Source + '&nbsp&nbsp&nbsp <b>To:</b> ' + this.responseModel.Destination
    //   + '&nbsp&nbsp&nbsp <b>Division:</b> ' + this.responseModel.Division + '&nbsp&nbsp&nbsp<b>Year:</b> ' + this.responseModel.Year
    //   + '<br/> '
    this.successText = '<b>Processed Using Batch File: </b><br/>' + this.responseModel.BatchFileInfo ? this.responseModel.BatchFileInfo : "none";
    this.alertClass = this.responseModel.DivBlocked == true ? 'alert-danger' : 'alert-success';
  }

  ResponseFailAction() {
    this.alertAreaText = 'Copy failed.';
    this.alertClass = 'alert-danger';
    this.successText = undefined;
  }

  formIsValid() {
    if (this.clientCopyModel.GetCopyFromToNumber() < 4 && this.clientCopyModel.Division && this.clientCopyModel.Year) {
      return true;
    }
    return false;
  }

}
